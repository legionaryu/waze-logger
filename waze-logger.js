var lzma = require("lzma");
var request = require("request");
var path = require("path");
var fs = require("fs");
var Qs = require("qs");
var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectId = require('mongodb').ObjectID;
var url = 'mongodb://localhost:27017/waze-logger';

var dirFrom = [
"Paulista",
"Aricanduva",
"Interlagos",
"EusebioMatoso",
"Santana",
];

var dirTo = [
"Ubatuba",
"Caraguatatuba",
"SaoSebastiao",
"Bertioga",
"Santos",
"Itanhaem",
"Peruibe",
];

var from = [
"x:-46.6529274+y:-23.5640265", // Av. Paulista, 1728 - Bela Vista, São Paulo - SP, Brasil
"x:-46.506725+y:-23.566416", // Shopping Aricanduva
"x:-46.7016284+y:-23.702306", // Av. Interlagos, 6457 - Interlagos, São Paulo - SP, Brasil
"x:-46.6961451+y:-23.5715409", // Av. Eusébio Matoso - Pinheiros, São Paulo - SP, Brasil
"x:-46.6263409+y:-23.49875219999999", // Travessa Danças Caipiras, 57, São Paulo - SP, Brasil
];

var to = [
"x:-45.0838529+y:-23.4336713", // Ubatuba, SP, Brasil
"x:-45.42414530000001+y:-23.6255903", // Caraguatatuba, SP, Brasil
"x:-45.4016534+y:-23.8063468", // Sao Sebastiao, SP, Brasil
"x:-46.0589521+y:-23.8064303", // Bertioga, SP, Brasil
"x:-46.3288865+y:-23.9678823", // Santos, SP, Brasil
"x:-46.78503850000001+y:-24.181841", // Itanhaem, SP, Brasil
"x:-47.0016573+y:-24.3124785", // Peruibe, SP, Brasil
];


var proxyList = [
"https://177.19.196.59:3130",
"http://200.138.65.125:8085",
"https://72.8.247.101:8080",
"https://66.192.70.198:3128",
"https://69.85.193.119:3128",
"http://66.76.24.115:3128",
"http://71.205.92.250:80",
"http://205.177.86.114:81",
"https://71.14.30.100:8080"
];

function logWazeRoutesFromCapital() {
    var i=0, j=0;
    // for (var i = 0; i < from.length; i++) {
        // for (var j = 0; j < to.length; j++) {
        var func_routes = function(i, j){
            var query = Qs.stringify({
                    from: from[i],
                    to: to[j],
                    at: "0",
                    returnJSON: "true",
                    returnGeometries: "false",
                    returnInstructions: "false",
                    timeout: "60000",
                    nPaths: "3",
                    clientVersion: "4.0.0",
                    options: "AVOID_TRAILS:t,ALLOW_UTURNS:t",
                }).replace(/%2B/g, "+");
            var uri = "https://www.waze.com/row-RoutingManager/routingRequest?"+query;
            console.log("Requesting route from "+dirFrom[i]+" to "+dirTo[j]);
            var start;
            console.log((start=new Date()).toISOString());
            request({
                // uri: "https://www.waze.com/row-RoutingManager/routingRequest?from=x:-46.6529274+y:-23.5640265&to=x:-45.0838529+y:-23.4336713&at=0&returnJSON=true&returnGeometries=false&returnInstructions=false&timeout=60000&nPaths=3&clientVersion=4.0.0&options=AVOID_TRAILS:t%2CALLOW_UTURNS:t",
                uri: uri,
                // uri: "https://www.waze.com/row-RoutingManager/routingRequest?from=x%3A-46.6529274+y%3A-23.5640265&to=x%3A-45.0838529+y%3A-23.4336713&at=0&returnJSON=true&returnGeometries=false&returnInstructions=false&timeout=60000&nPaths=3&clientVersion=4.0.0&options=AVOID_TRAILS%3At%2CALLOW_UTURNS%3At",
                method: "GET",
                headers: {
                    "Host": "www.waze.com",
                    "Cache-Control": "max-age=0",
                    "Accept-Encoding": "gzip, deflate",
                    "Connection": "keep-alive",
                    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:43.0) Gecko/20100101 Firefox/43.0",
                },
                // qs: {
                //     from: from[i],
                //     to: to[j],
                //     at: "0",
                //     returnJSON: "true",
                //     returnGeometries: "false",
                //     returnInstructions: "false",
                //     timeout: "60000",
                //     nPaths: "3",
                //     clientVersion: "4.0.0",
                //     options: "AVOID_TRAILS:t,ALLOW_UTURNS:t",
                // }
            }, function(error, response, body) {
                var end;
                console.log((end=new Date()).toISOString(), dirFrom[i]+"."+dirTo[j]+" response delay:", (end-start));
                if(error){
                    return console.log('Error:', error);
                }
                if(response.statusCode == 200){
                	var data = JSON.parse(response.body);
                    data.timestamp = new Date();
                    var f = i, t = j;
                    MongoClient.connect(url, function(err, db) {
                      assert.equal(null, err);
                      db.collection(dirFrom[f]+'.'+dirTo[t]).insertOne(data , function() {
                        assert.equal(err, null);
                        console.log("successfully inserted data on \'"+dirFrom[f]+"."+dirTo[t]+"\'");
                        db.close();
                      });
                    });
                    
                    // lzma.compress(response.body, 2, function(buffer, err){
                    //     var now = new Date();
                    //     var log_path;
                    //     makeDir((log_path=path.join(__dirname, dirFrom[i])));
                    //     makeDir((log_path=path.join(log_path, dirTo[j])));
                    //     fs.writeFile((log_path=path.join(log_path,now.toISOString().replace(/[:\.]/g, "")+".lzma")), buffer, function (err) {
                    //         if (err) console.log(err);
                    //         console.log("Compressed route log successfully created on "+log_path);
                    //     });
                    // });
                } else {
                    console.log(typeof(response.statusCode), response.statusCode, body);
                }
                if(++i >= from.length) {
                    j++;
                }
                i %= from.length;
                j %= to.length;
                setTimeout(func_routes, 100 + Math.random() * 50, i, j);
            });
            // setTimeout(func_routes, 2570, i, j);
        };
        func_routes(i,j);
        // }
    // }
}


function makeDir(str_path) {
    try {
        fs.mkdirSync(str_path);
    } catch(ex) {
        if(ex.errno != -4075) {
            throw ex;
        }
    }
}

logWazeRoutesFromCapital();

//setInterval(, 15 * 60000)

// "https://www.waze.com/row-RoutingManager/routingRequest?from=x%3A-46.6529274+y%3A-23.5640265&to=x%3A-45.0838529+y%3A-23.4336713&at=0&returnJSON=true&returnGeometries=false&returnInstructions=false&timeout=60000&nPaths=3&clientVersion=4.0.0&options=AVOID_TRAILS%3At%2CALLOW_UTURNS%3At"
// "https://www.waze.com/row-RoutingManager/routingRequest?from=x%3A-46.6529274+y%3A-23.5640265&to=x%3A-45.0838529%2By%3A-23.4336713&at=0&returnJSON=true&returnGeometries=false&returnInstructions=false&timeout=60000&nPaths=3&clientVersion=4.0.0&options=AVOID_TRAILS%3At%2CALLOW_UTURNS%3At"
// "https://www.waze.com/row-RoutingManager/routingRequest?from=x%3A-46.6529274%2By%3A-23.5640265&to=x%3A-45.0838529%2By%3A-23.4336713&at=0&returnJSON=true&returnGeometries=false&returnInstructions=false&timeout=60000&nPaths=3&clientVersion=4.0.0&options=AVOID_TRAILS%3At%2CALLOW_UTURNS%3At"

